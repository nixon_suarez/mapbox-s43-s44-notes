import toNum from '../../helpers/toNum'
import { Doughnut } from 'react-chartjs-2'

export default function top({data}) {
  console.log(data)

  const countriesStats = data.countries_stat

  console.log(countriesStats);

  const countriesCases = countriesStats.map(countryStat => {
    return {
      name: countryStat.country_name,
      cases: toNum(countryStat.cases)
    }
  })

  console.log(countriesCases)

  // [USA, Philippines, ]

  const countriesArray = countriesCases.sort((a, b) => {
    if(a.cases < b.cases){
      return 1
    }else if (a.cases > b.cases){
      return -1 // move to the left
    }else{
      return 0 // nothing will happen
    }
  }).slice(0, 10)

  console.log(countriesArray)

  let countryNames = [];

  countriesArray.forEach(country => {
    return countryNames.push(country.name)
  })

  console.log(countryNames)

  let countryCases = [];

  countriesArray.forEach(country => {
    return countryCases.push(country.cases)
  })

  console.log(countryCases)  

  return(
    <React.Fragment>
      <h1>10 Countries With The Highest Number of Cases</h1>
      <Doughnut data={{
        datasets: [{
            data: [...countryCases],
            backgroundColor: ["red", "yellow", "orange", "blue", "green", "indigo", "violet", "black", "gray", "white"]
        }],
            labels: [...countryNames]
      }} redraw={ false }/>
    </React.Fragment>  
  )
}

export async function getStaticProps() {
  //fetch data from the /courses API endpoint    
  const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
    "method": "GET",
    "headers": {
    "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
    "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
    }
  })
  const data = await res.json()

  //return the props
  return { 
    props: {
      data
    }
  }
}